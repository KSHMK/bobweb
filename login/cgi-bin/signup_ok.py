#!/usr/bin/python
PASS_PATH = "/var/www/passwd"
from os import environ
from Cookie import SimpleCookie
import cgi, cgitb
import hashlib

def isid(user_id):
    global PASS_PATH
    try:
        fd = open(PASS_PATH,'r')
        lines = fd.readlines()
        fd.close()
    except:
        print "<script>alert('Call Admin!');</script><meta http-equiv='refresh' content='0;url=/login/signup.php'>"
        exit()
    for line in lines:
        if line.find(":"+user_id+":") != -1:
            return True

    #no find
    return False

def createaccount(user_id,user_pw,user_name):
    global PASS_PATH
    account = ":"+user_id+":"+user_pw+":"+user_name+":0:\n"
    try:
        fd = open(PASS_PATH,'a')
        fd.write(account)
        fd.close()
    except:
        print "<script>alert('Call Admin!');</script><meta http-equiv='refresh' content='0;url=/login/signup.php'>"
        exit()
 
def main():
    #set content type
    print "Content-type:text/html\r\n\r\n"
    
    #get data from form
    form = cgi.FieldStorage()
    
    #get user id and pw
    try:
        user_id = form.getvalue('user_id')
        user_pw = form.getvalue('user_pw')
        user_pw = str(hashlib.sha1(user_pw).hexdigest())
        user_name = form.getvalue('user_name')
    except:
        print "<script>alert('no hack');</script><meta http-equiv='refresh' content='0;url=/login/signup.php'>"
        exit()
    
    if user_id == None or user_pw == None or user_name == None:
        print "<script>alert('Something Missing');</script><meta http-equiv='refresh' content='0;url=/login/signup.php'>"
        exit()
    

    #is id
    if isid(user_id) == True:
        print "<script>alert('Duplicate ID');</script><meta http-equiv='refresh' content='0;url=/login/signup.php'>"
        exit()
    
    createaccount(user_id,user_pw,user_name)
    print "<script>alert('Signup OK!');</script><meta http-equiv='refresh' content='0;url=/'>"
    exit()

if __name__ == "__main__":
    main()
