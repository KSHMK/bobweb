<!DOCTYPE html>
<meta charset="utf-8" />
<html>
    <head>
        <title>Login</title>
    </head>
    <body>
    <style>
        .container { width: 70%; height: 70%; margin: 40px auto;}
        .outer { display: table; width: 100%; height: 100%;}
        .inner { display: table-cell; vertical-align: middle; text-align: center;}
        .centered { display: table; margin-left: auto; margin-right: auto;}
    </style>
<?php
session_start();
if(isset($_SESSION['user_id']) && isset($_SESSION['user_name'])){
    echo "<meta http-equiv='refresh' content='0;url=/'>";
    exit;
}
?>
    <br><br><br>
    <div class="container">
    <div class="outer">
    <div class="inner">
    <div class="centered">
    <form method='post' action='/cgi-bin/login_ok.py'>
        <table>
            <tr>
                <td>ID</td>
                <td><input type='text' name='user_id' tabindex='1'/></td>
                <td rowspan='2'><input type='submit' tabindex='3' value='Login' style='height:50px'/></td>
                <td rowspan='2'><input type='button' name='buttons' value='Signup' style='height:50px' onclick="location.href='./signup.php';";/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type='password' name='user_pw' tabindex='2' /></td>
            </tr>
        </table>
    </form>
    </div></div></div></div>
    </body>
</html>
