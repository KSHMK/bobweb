<!DOCTYPE html>
<meta charset="utf-8" />
<html>
    <head>
        <title>Signup</title>
    </head>
    <body>
    <style>
        .container { width: 70%; height: 70%; margin: 100px auto;}
        .outer { display: table; width: 100%; height: 100%;}
        .inner { display: table-cell; vertical-align: middle; text-align: center;}
        .centered { display: table; margin-left: auto; margin-right: auto;}
    </style>
    <div class="container">
    <div class="outer">
    <div class="inner">
    <div class="centered">
    <form method='post' action='/cgi-bin/signup_ok.py'>
        <table>
            <tr>
                <td>ID</td>
                <td><input type='text' name='user_id' tabindex='1'/></td>
                <td rowspan='3'><input type='submit' tabindex='4' value='Signup' style='height:75px'/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type='password' name='user_pw' tabindex='2' /></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type='text' name='user_name' tabindex='3' /></td>
            </tr>
        </table>
    </form>
    </div></div></div></div>
    </body>
</html>
