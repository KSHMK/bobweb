#!/usr/bin/python
SESS_PATH = "/var/lib/php/sessions/"
PASS_PATH = "/var/www/passwd"
from os import environ
from Cookie import SimpleCookie
import cgi, cgitb
import hashlib

def save_session(phpsessid,name,value):
    global SESS_PATH
    add = name+"|s:"+str(len(value))+":\""+value+"\";"
    phpsessid = phpsessid.replace("/","")
    phpsessid = phpsessid.replace(".","")
    try:
        fd = open(SESS_PATH+"sess_"+phpsessid,'a')
        fd.write(add)
        fd.close()
    except:
        print "<script>alert('Call Admin!');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()
        

def checkid(user_id,user_pw):
    global PASS_PATH
    try:
        fd = open(PASS_PATH,'r')
        lines = fd.readlines()
        fd.close()
    except:
        print "<script>alert('Call Admin!');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()
        
    for line in lines:
        if line.find(":"+user_id+":"+user_pw+":") != -1:
            try:
                (user_id,user_pw,user_name,isadmin) = line[1:-2].split(":")
            except:
                return (None, None)
            return (user_name,isadmin)

    #no find
    return (None,None)

def main():
    #set content type
    print "Content-type:text/html\r\n\r\n"
    
    #get data from form
    form = cgi.FieldStorage()
    
    #get user id and pw
    try:
        user_id = form.getvalue('user_id')
        user_pw = form.getvalue('user_pw')
        user_pw = str(hashlib.sha1(user_pw).hexdigest())
    except:
        print "<script>alert('Something Missing!');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()
    
    if user_id == None or user_pw == None:
        print "<script>alert('Something Missing!');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()
        
    #get phpsessid
    if environ.has_key('HTTP_COOKIE'):
        session = SimpleCookie(environ['HTTP_COOKIE'])
        try:
            phpsessid = session['PHPSESSID'].value
        except:
            print "<script>alert('no hack');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
            exit()
    else:
        print "<script>alert('no hack');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()

    #check id
    (user_name,isadmin) = checkid(user_id,user_pw)
    
    #Nope
    if user_name == None or isadmin == None:
        print "<script>alert('No ID or Password is incorrect!');</script><meta http-equiv='refresh' content='0;url=/login/login.php'>"
        exit()

    save_session(phpsessid,"user_id",user_id)
    save_session(phpsessid,"user_name",user_name)
    save_session(phpsessid,"user_pw",user_pw)
    save_session(phpsessid,"isadmin",isadmin)
    print "<script>alert('"+user_name+" Login OK!');</script><meta http-equiv='refresh' content='0;url=/'>"
    exit()

if __name__ == "__main__":
    main()
