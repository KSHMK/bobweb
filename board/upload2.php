<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
</head>
<body>
<?php
	$uploaddir = '/var/www/html/board/files/';
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
        $FileType = pathinfo($uploadfile, PATHINFO_EXTENSION);

        $BlackList = array("phps", "in", "shtml", "cgi", "phtml", "pl", "py", "rb", "php", "js", "jsp", "html", "html", "asp", "aspx",  "cer", "cdx", "asa", "war");
        $Temp = $FileType;
        $Temp = preg_replace ("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $Temp);
        $FileType = $Temp;

        if (in_array($FileType, $BlackList)) {
                ?>
                        <script>
                                alert("업로드 할 수 없는 확장자입니다.");
                                history.back();
                        </script>
                <?php
                        exit;
        }

        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
           ?>
                <script>
                        alert("파일을 업로드 했습니다.");
                        location.href='./index.php';
                </script>
        <?php
        } else {
               ?>
                        <script>
                                alert("파일 업로드를 실패했습니다.");
                                history.back();
                        </script>
        <?php
        }
                exit;
        ?>
</body>
</html>

