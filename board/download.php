<!DOCTYPE html>
<meta charset="utf-8" />
<html>
    <head>
        <title>파일다운로드</title>
    </head>
    <body>
    <style>
        .container { width: 70%; height: 70%; margin: 40px auto;}
        .outer { display: table; width: 100%; height: 100%;}
        .inner { display: table-cell; vertical-align: middle; text-align: center;}
        .centered { display: table; margin-left: auto; margin-right: auto;}
    </style>
    <div class="container">
    <div class="outer">
    <div class="inner">
    <div class="centered">
    <form method='post' action='./download2.php'>
        <table>
            <tr>
                <td>파일이름</td>
                <td><input type='text' name='fname' tabindex='1'/></td>
                <td rowspan='2'><input type='submit' tabindex='2' value='입력' /></td>
            </tr>
        </table>
    </form>
    </div></div></div></div>
    </body>
</html>
