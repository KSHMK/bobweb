<?php
        require_once("../dbconfig.php");
        
        session_start();
        if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
                echo "<meta http-equiv='refresh' content='0;url=/'>";
                exit;
        }
        
        $bNo = $_GET['bno'];

        if(!empty($bNo) && empty($_COOKIE['board_free_' . $bNo])) {
                $sql = 'update board_free set b_hit = b_hit + 1 where b_no = ' . $bNo;
                $result = $db->query($sql);
                if(empty($result)) {
                        ?>
                        <script>
                                alert('오류가 발생했습니다.');
                                history.back();
                        </script>
                        <?php
                } else {
                        setcookie('board_free_' . $bNo, TRUE, time() + (60 * 60 * 24), '/');
                }
        }

        $sql = 'select b_title, b_content, b_date, b_hit, b_id from board_free where b_no = ' . $bNo;
        $result = $db->query($sql);
        $row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
        <title>자유게시판</title>
        <link rel="stylesheet" href="./css/normalize.css" />
        <link rel="stylesheet" href="./css/board.css" />
        <script src="./js/jquery-2.1.3.min.js"></script>
</head>
<body>
        <center>
        <article class="boardArticle">
                <h3>자유게시판 글쓰기</h3><br>
                <div id="boardView">
                        <h1 id="boardTitle">제목: <?php echo $row['b_title']?></h1>
                        <div id="boardInfo"><br>
                                <span id="boardID">작성자: <?php echo $row['b_id']?></span>
                                <span id="boardDate">작성일: <?php echo $row['b_date']?></span>
                                <span id="boardHit">조회: <?php echo $row['b_hit']?></span>
                        </div>
                        <br>
                        <div id="boardContent"><div style = "font-size: 2.0em";><?php echo  $row['b_content']?></div><br><br>
                        <div class="btnSet">
                        <?php if($_SESSION['user_id'] === $row['b_id'] ){ ?>
                                <input type="button" value="수정" onClick="location.href='./write.php?bno=<?php echo $bNo?>'">
                        <?php } if($_SESSION['user_id'] === $row['b_id'] || $_SESSION['isadmin'] === "1") { ?>
                                <input type="button" value="삭제" onClick="location.href='./delete.php?bno=<?php echo $bNo?>'">
                        <?php } ?>
                                <input type="button" value="목록" onClick="location.href='./'">
                        </div>
                        <br> <br>
                </div>
        </center>
        </article>
</body>
</html>
