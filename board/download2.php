<html>
<head>
</head>
<body>
<?php
function mb_basename($path) { return end(explode('/',$path));}
function utf2euc($str){ return iconv("UTF-8","cp949//IGNORE",$str);}
function is_ie() {
    if(!isset($_SERVER['HTTP_USER_AGENT'])) return false;
    if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== false) return true;
    if(strpos($_SERVER['HTTP_USER_AGENT'],'Windows NT 6.1') !== false) return true;
    return false;
}
$downloaddir = '/var/www/html/board/files/';
if(!isset($_POST['fname'])){
?>
<script>history.back();</script>
<?php
    exit;
}
$files = $downloaddir.$_POST["fname"];
if(strpos($files, ".php") !== false){
?>
<script>alert('no hack');history.back();</script>
<?php
    exit;
}
$filepath = htmlspecialchars($files);
$filesize = filesize($filepath);
$filename = mb_basename($filepath);
if(is_ie()) $filename = utf2euc($filename);

header("Pragma: public");
header("Expires: 0");
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: $filesize");

ob_clean();
flush();
readfile($filepath);
?>
</body>
</html>


