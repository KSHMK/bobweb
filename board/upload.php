<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>파일업로드</title>
</head>   
<body>
    <style>
        .container { width: 70%; height: 70%; margin: 100px auto;}
        .outer { display: table; width: 100%; height: 100%;}
        .inner { display: table-cell; vertical-align: middle; text-align: center;}
        .centered { display: table; margin-left: auto; margin-right: auto;}
    </style>
    <div class="container">
    <div class="outer">
    <div class="inner">
    <div class="centered">
    <form enctype="multipart/form-data" action="upload2.php" method="POST">
		<input name="userfile" type="file" />
		<input type="submit" value="파일 업로드" />
    </form>
    </div></div></div></div>
</body>
</html>
