<!DOCTYPE html>
<meta charset="utf-8" />
<html>
    <head>
        <title>Admin</title>
    </head>
    <style>  
    .mytable { border-collapse:collapse; }  
    .mytable th, .mytable td { border:1px solid black; }
    </style>
    <body>
<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name']) || !isset($_SESSION['isadmin'])){
    echo "<meta http-equiv='refresh' content='0;url=/'>";
    exit;
}
if($_SESSION['isadmin']!=1){
    echo "<meta http-equiv='refresh' content='0;url=/'>";
    exit;
}
$fp = fopen("/var/www/passwd","r");
$fr = fread($fp,filesize("/var/www/passwd"));
fclose($fp);
$accounts = explode("\n",$fr);
?>
    <table class="mytable">
        <tr>
            <td>ID</td><td>PW</td><td>NAME</td><td>ISADMIN</td>
        </tr>
<?php
for($i=0;$i<sizeof($accounts);$i++){
    $account = explode(":",$accounts[$i]);
    echo "<tr>\n";
    for($j=1;$j<sizeof($account)-1;$j++)
        echo "<td>".$account[$j]."</td>";
    echo "</tr>\n";
}
?>
    </table>
    <input type="button" value="Reset Account" onClick="location.href='./reset.php'">
    <input type="button" value="Reset Board" onClick="location.href='./resetboard.php'"> 
    <input type="button" value="Reset File" onClick="location.href='./resetfile.php'"> 
    <input type="button" value="Board" onClick="location.href='/board/'"> 
    <br>
    KEY: I_H4TE_SQL_S0_i_USE_PyThOn
    <br>
    <h1>Reset Account Please~!</h1>
    </body>
</html>


